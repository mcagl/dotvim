# My Vim setup

This setup uses junegunn's `vim-plug` plugin manager, and I'm mainly targetting my needs for python and django development.


## FIRST TIME INSTALLATION
* BACKUP YOUR **CURRENT** CONFIGURATION (don't blame me if you mess everything.)
* Remove `~/.vimrc`, `~/.gvimrc`, and the `~/.vim/` directory
* `git clone https://gitlab.com/mcagl/dotvim.git ~/.vim`
* `touch ~/.vim/private_settings_do_not_commit.vim`
* `mkdir -p ~/.vim/vim_plugins/`
* `git clone https://github.com/junegunn/vim-plug ~/.vim/vim_plugins/vim-plug.git`
* `ln -s ~/.vim/vimrc ~/.vimrc`
* `vim +PlugInstall`

And everything should be OK!

When you want to upgrade `vim-plug` and the plugins:

* `vim +PlugUpgrade +PlugUpdate`

If you install a new plugin:

* `vim +PlugInstall`

If you remove a plugin:

* `vim +PlugClean`


## PLUGINS THAT I USE

Please note that the languages categories are in alphabetical (not importance) order :-)


### General

* [vim-colorschemes](https://github.com/flazz/vim-colorschemes)
* [fzf](https://github.com/junegunn/fzf)
* [fzf.vim](https://github.com/junegunn/fzf.vim)
* [indentLine](https://github.com/Yggdroot/indentLine)
* [vim-licenses](https://github.com/mcagl/vim-licenses) (an old fork of mine)
* [lightline.vim](https://github.com/itchyny/lightline.vim)
* [lightline-bufferline](https://github.com/mengelbrecht/lightline-bufferline)
* [nerdcommenter](https://github.com/scrooloose/nerdcommenter)
* [vim-ripgrep](https://github.com/jremmen/vim-ripgrep)
* [vim-signature](https://github.com/kshenoy/vim-signature)
* [vim-snippets](https://github.com/honza/vim-snippets)
* [supertab](https://github.com/ervandew/supertab)
* [vim-surround](https://github.com/tpope/vim-surround)
* [syntastic](https://github.com/vim-syntastic/syntastic)
* [ultisnips](https://github.com/SirVer/ultisnips)
* [wildfire.vim](https://github.com/gcmt/wildfire.vim)


### Git

* [vim-fugitive](https://github.com/tpope/vim-fugitive)
* [vim-gitgutter](https://github.com/airblade/vim-gitgutter)
* [lightline-hunks](https://github.com/sinetoami/lightline-hunks)


### CSS

* [vim-css-color](https://github.com/ap/vim-css-color)


### HTML

* [vim-closetag](https://github.com/alvan/vim-closetag)
* [html5.vim](https://github.com/othree/html5.vim)
* [sparkup](https://github.com/rstacruz/sparkup)


### Javascript

* [vim-javascript](https://github.com/pangloss/vim-javascript)
* [vim-javascript-syntax](https://github.com/jelera/vim-javascript-syntax)


### Po

* [po.vim--gray](https://github.com/vim-scripts/po.vim--gray)


### Python

* [jedi-vim](https://github.com/davidhalter/jedi-vim)
* [vim-python-pep8-indent](https://github.com/Vimjas/vim-python-pep8-indent)


### RestructuredText / MarkDown

* [previm](https://github.com/previm/previm)


LICENSE: [WTFPL](http://www.wtfpl.net)
