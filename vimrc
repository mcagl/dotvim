"""""""""""""""""""""""""""""""""""""""""""""""""
" VimPlug
call plug#begin('~/.vim/vim_plugins')

" Plugins
" In alphabetical order
Plug 'alvan/vim-closetag'
Plug 'flazz/vim-colorschemes'
Plug 'ap/vim-css-color'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-gitgutter'
Plug 'othree/html5.vim'
Plug 'Yggdroot/indentLine'
Plug 'pangloss/vim-javascript'
Plug 'jelera/vim-javascript-syntax'
Plug 'davidhalter/jedi-vim'
Plug 'mcagl/vim-licenses'
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'sinetoami/lightline-hunks'
Plug 'scrooloose/nerdcommenter'
Plug 'vim-scripts/po.vim--gray'
Plug 'previm/previm'
Plug 'Vimjas/vim-python-pep8-indent'
Plug 'jremmen/vim-ripgrep'
Plug 'kshenoy/vim-signature'
Plug 'honza/vim-snippets'
Plug 'rstacruz/sparkup'
Plug 'ervandew/supertab'
Plug 'tpope/vim-surround'
Plug 'vim-syntastic/syntastic'
Plug 'dhruvasagar/vim-table-mode'
Plug 'SirVer/ultisnips'
Plug 'gcmt/wildfire.vim'
call plug#end()
filetype plugin indent on
""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""
" Vim Configuration (not about plugins)
""""""""""""""""""""""""""""""""""""""""""""""""
" Fix Jedi + SuperTab autocompletion
set nopaste

" Set to 256 colors
set t_Co=256

" Use mouse
set mouse=a

" Keep backup of files in a hidden subdirectory under $VIMRUNTIME
set backup
set backupdir=$HOME/.vim/.backup_files
set directory=$HOME/.vim/.swp_files

" Default file encoding
set encoding=utf8
set fileencoding=utf8

" Default whitespace managing
set ts=4 sts=4 sw=4 expandtab

" https://stackoverflow.com/questions/5774824/vim-how-to-set-filetype-for-none-extension-file-by
" https://news.ycombinator.com/item?id=20098691
" Vim/Neovim Arbitrary Code Execution via Modelines
set nomodeline

" Highlight column. PEP8 80 chars seems a little anacronistic...
set colorcolumn=101

" Syntax highlighting
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch    " Highlight search
    set incsearch   " Search as you type
endif

" GVIM font
if has("gui_running")
    set guifont=Inconsolata\ for\ Powerline\ 11
endif

" Define filetype for every file with these extensions
autocmd BufNewFile,BufRead *.rss,*.atom setfiletype xml

" Take care of django files
autocmd BufNewFile,BufRead *.html call s:FThtmldjango()
autocmd BufNewFile,BufRead *.py call s:FTpydjango()

func! s:FThtmldjango()
  let n = 1
  while n < 30 && n < line("$")
    if getline(n) =~ '.*{%.*'
      set ft=htmldjango
      return
    endif
    let n = n + 1
  endwhile
  set ft=html
endfunc

func! s:FTpydjango()
  let n = 1
  while n < 30 && n < line("$")
    if getline(n) =~ '.*django.*'
      set ft=python.django
      return
    endif
    let n = n + 1
  endwhile
  set ft=python
endfunc

" Set whitespace managing for every filetype, overriding standard
" Configure vim to be PEP8 compliant when editing Python code
autocmd FileType python,python.django setlocal ts=4 sts=4 sw=4 expandtab cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd FileType html,xml,htmldjango,javascript setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType makefile setlocal ts=4 sts=4 sw=4 noexpandtab
" Automatically wrap at 3 columns from the window border
autocmd FileType tex setlocal wm=3

" Automatically remove trailing whitespace before saving
" http://stackoverflow.com/a/1618401/1651545
fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
autocmd FileType c,cpp,java,php,ruby,python,html,htmldjango,javascript,rst,markdown autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

" Various settings
" http://stackoverflow.com/a/2559262/1651545
let mapleader=','                   " <leader> is the comma
set nowrap                          " Don't wrap lines
set scrolloff=2                     " 2 lines above/below cursor when scrolling
set showcmd                         " Shows the command at the bottom of the window
set showmatch                       " Show matching bracket
set matchtime=5                     " Show matching bracket for 0.5 seconds
" Match also < > for html, htmldjango and xml
autocmd FileType html,htmldjango,xml setlocal matchpairs+=<:>
set number                          " Use line numbers
set numberwidth=4                   " Minimum field width for line numbers
set hidden                          " Allow to hid modified buffers
" https://stackoverflow.com/questions/526858/how-do-i-make-vim-do-normal-bash-like-tab-completion-for-file-names
set wildmode=longest,list,full
set wildmenu
" https://mg.pov.lt/vim/vimrc
set suffixes+=.pyc
set wildignore+=*.pyc
" Better line wraps: http://www.bestofvim.com/tip/better-line-wraps/
set showbreak=↪


""""""""""""""""""""""""""""""""""""""""""""""""
" COLORSCHEME
""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable
set background=dark
" solarized badwolf kolor
colorscheme desert256
" https://rsapkf.netlify.com/blog/enabling-italics-vim-tmux
" https://news.ycombinator.com/item?id=22716612
" To have italics in vim inside tmux, write this in ~/.tmux.conf
" set -g default-terminal "tmux-256color"
highlight Comment cterm=italic
" http://vim.wikia.com/wiki/Fix_syntax_highlighting
noremap <F12> <Esc>:syntax sync fromstart<CR>
inoremap <F12> <C-o>:syntax sync fromstart<CR>


""""""""""""""""""""""""""""""""""""""""""""""""
" AUTORELOAD VIMRC
""""""""""""""""""""""""""""""""""""""""""""""""
" https://vim.fandom.com/wiki/Change_vimrc_with_auto_reload
augroup reload_vimrc
    autocmd!
    autocmd BufWritePost .vimrc source %
    autocmd BufWritePost vimrc source %
augroup END


""""""""""""""""""""""""""""""""""""""""""""""""
" MOVE BETWEEN BUFFERS AND WINDOWS
""""""""""""""""""""""""""""""""""""""""""""""""
map <F2> :bprevious<CR>
map <F3> :bnext<CR>
map <F4> :bdelete<CR>
" Move through windows with CTRL + arrows
nnoremap <silent> <C-left> <C-W>h
nnoremap <silent> <C-right> <C-W>l
nnoremap <silent> <C-up> <C-W>k
nnoremap <silent> <C-down> <C-W>j


""""""""""""""""""""""""""""""""""""""""""""""""
" OMNICOMPLETION
""""""""""""""""""""""""""""""""""""""""""""""""
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags


""""""""""""""""""""""""""""""""""""""""""""""""
" SHORTCUT TO MAKE GVIM FULLSCREEN VIM-LIKE
""""""""""""""""""""""""""""""""""""""""""""""""
" http://askubuntu.com/questions/2140/is-there-a-way-to-turn-gvim-into-fullscreen-mode
" http://www.windowslinuxosx.com/q/answers-how-can-i-open-gvim-in-full-screen-mode-in-gnome-264693.html
map <silent> <F12> :call system("wmctrl -ir " . v:windowid . " -b toggle,fullscreen")<CR>
set guioptions-=T guioptions-=m


""""""""""""""""""""""""""""""""""""""""""""""""
" PERFORMANCE FIX
""""""""""""""""""""""""""""""""""""""""""""""""
" Trying to fix vim bad performance wrt scrolling when many big files are open
" https://stackoverflow.com/questions/307148/vim-scrolling-slowly
set ttyfast
set lazyredraw


""""""""""""""""""""""""""""""""""""""""""""""""
" DISABLE THE ZZ COMMAND
""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap ZZ <nop>


""""""""""""""""""""""""""""""""""""""""""""""""
" OTHER REMAPS
""""""""""""""""""""""""""""""""""""""""""""""""
" Selects last inserted text
noremap <leader>l `[v`]


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PLUGIN SETTINGS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""
" LIGHTLINE
""""""""""""""""""""""""""""""""""""""""""""""""
set laststatus=2                    " Use 2 lines for the statusbar
set showtabline=2
" https://github.com/itchyny/lightline.vim#mode-names-are-too-long-can-i-use-shorter-mode-names
let g:lightline = {
\   'mode_map': {
\       'n' : 'N',
\       'i' : 'I',
\       'R' : 'R',
\       'v' : 'V',
\       'V' : 'VL',
\       "\<C-v>": 'VB',
\       'c' : 'C',
\       's' : 'S',
\       'S' : 'SL',
\       "\<C-s>": 'SB',
\       't': 'T',
\   },
\   'active': {
\       'left': [ [ 'mode', 'paste' ],
\                 [ 'lightline_hunks', 'readonly', 'filename', 'modified' ] ],
\       'right': [ [ 'lineinfo' ],
\                  [ 'percent' ],
\                  [ 'syntastic_integration', 'fileformat', 'fileencoding', 'filetype' ] ],
\   },
\   'tabline': {
\       'left': [ [ 'buffers' ] ],
\       'right': [ [ 'close' ] ],
\   },
\   'component_expand': {
\       'buffers': 'lightline#bufferline#buffers',
\   },
\   'component_type': {
\       'buffers': 'tabsel',
\   },
\   'component_function': {
\       'lightline_hunks': 'lightline#hunks#composer',
\       'syntastic_integration': 'SyntasticStatuslineFlag',
\   },
\ }


""""""""""""""""""""""""""""""""""""""""""""""""
" RIPGREP
""""""""""""""""""""""""""""""""""""""""""""""""
" https://www.mattlayman.com/blog/2019/supercharging-vim-blazing-fast-search/
let g:rg_command = 'rg --vimgrep -S'


""""""""""""""""""""""""""""""""""""""""""""""""
" SUPERTAB
""""""""""""""""""""""""""""""""""""""""""""""""
let g:SuperTabDefaultCompletionType = "context"

" Add the following snippet to your vimrc to escape insert mode immediately
if ! has('gui_running')
    set ttimeoutlen=10
    augroup FastEscape
        autocmd!
        au InsertEnter * set timeoutlen=0
        au InsertLeave * set timeoutlen=1000
    augroup END
endif


""""""""""""""""""""""""""""""""""""""""""""""""
" JEDI
""""""""""""""""""""""""""""""""""""""""""""""""
" Disable docstring popup (hopefully...)
" Found here: https://github.com/davidhalter/jedi-vim#i-dont-want-the-docstring-window-to-popup-during-completion
autocmd FileType python,python.django setlocal completeopt-=preview
let g:jedi#popup_on_dot = 0
let g:jedi#completions_enabled = 1
let g:jedi#popup_select_first = 0
let g:jedi#show_call_signatures = 0
let g:jedi#use_tabs_not_buffers = 0
" https://github.com/davidhalter/jedi-vim/commit/ac1615c647da766534759ece8b253e3a6dd543ee
let g:jedi#smart_auto_mappings = 0


""""""""""""""""""""""""""""""""""""""""""""""""
" GITGUTTER
""""""""""""""""""""""""""""""""""""""""""""""""
hi GitGutterAdd ctermfg=green guifg=#00ff00 gui=bold
hi GitGutterChange ctermfg=yellow guifg=#ffff00 gui=bold
hi GitGutterDelete ctermfg=red guifg=#ff0000 gui=bold
hi GitGutterChangeDelete ctermfg=yellow guifg=#ffa500 gui=bold
nmap <leader>m <Plug>(GitGutterPrevHunk)
nmap <leader>. <Plug>(GitGutterNextHunk)
" https://github.com/airblade/vim-gitgutter#sign-column
" vim-gitgutter used to do this by default:
highlight! link SignColumn LineNr
" Other tweaks for gitgutter
set updatetime=100


""""""""""""""""""""""""""""""""""""""""""""""""
" SYNTASTIC
""""""""""""""""""""""""""""""""""""""""""""""""
" Disable syntastic by default, except for a whitelist of formats
" see: https://github.com/scrooloose/syntastic/issues/101
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': ['python', 'sh'], 'passive_filetypes': [] }
" Use only tidy and no online service to check HTML
" https://github.com/scrooloose/syntastic/issues/485
let g:syntastic_html_checkers=['tidy']
" flake8 understands newer python syntax (async, type hints, and so on)
" whereas pyflakes does not.
let g:syntastic_python_checkers=['flake8']
" Disable annoying pep8 checks
" E501: line too long
let g:syntastic_python_flake8_args = '--ignore=E501'
let g:syntastic_sh_checkers=['shellcheck']
let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


""""""""""""""""""""""""""""""""""""""""""""""""
" ULTISNIPS
""""""""""""""""""""""""""""""""""""""""""""""""
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my_snippets"]


""""""""""""""""""""""""""""""""""""""""""""""""
" FZF
""""""""""""""""""""""""""""""""""""""""""""""""
" https://stackoverflow.com/a/51457124
silent! nmap <C-p> :GFiles<CR>
nmap <leader>f :Files<CR>
" This needs fugitive to work
nmap <leader>c :Commits<CR>


""""""""""""""""""""""""""""""""""""""""""""""""
" PREVIM
""""""""""""""""""""""""""""""""""""""""""""""""
let g:previm_open_cmd = 'xdg-open'


""""""""""""""""""""""""""""""""""""""""""""""""
" DON'T COMMIT/PUSH THIS FILE TO REMOTE PUBLIC REPOS
""""""""""""""""""""""""""""""""""""""""""""""""
:source ~/.vim/private_settings_do_not_commit.vim
